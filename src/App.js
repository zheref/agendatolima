/**
 * @public
 * @module
 */
angular.module('agendatolima', ['ionic', 'agendatolima.controllers', 'agendatolima.services', 'agendatolima.providers', 'agendatolima.resources'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if(window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        moment.lang('es');
        window.GMAPS_READY = false;
    })

    .config(function($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'AppController'
            })

            .state('app.search', {
                url: "/search",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/search.html"
                    }
                }
            })

            .state('app.publicacion', {
                url: "/publicacion/:id",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/publicacion.html",
                        controller: 'PublicacionController'
                    }
                }
            })

            .state('app.publicaciones', {
                url: "/publicaciones",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/publicaciones.html",
                        controller: 'PublicacionesController'
                    }
                }
            })

            .state('app.eventos', {
                url: "/eventos",
                views: {
                    'menuContent': {
                        templateUrl: "templates/eventos.html",
                        controller: 'EventosController'
                    }
                }
            })

            .state('app.lugares', {
                url: "/lugares",
                views: {
                    'menuContent': {
                        templateUrl: "templates/lugares.html",
                        controller: 'LugaresController'
                    }
                }
            })

            .state('app.busqueda', {
                url: "/busqueda",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/busqueda.html",
                        controller: 'BusquedaController'
                    }
                }
            })

            .state('app.login', {
                url: "/login",
                views: {
                    'menuContent': {
                        templateUrl: "templates/login.html",
                        controller: 'AuthController'
                    }
                }
            })

            .state('app.signup', {
                url: "/signup",
                views: {
                    'menuContent': {
                        templateUrl: "templates/signup.html",
                        controller: 'SignupController'
                    }
                }
            })

            .state('app.categorias', {
                url: "/categorias",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/categorias.html",
                        controller: 'CategoriasController'
                    }
                }
            })

            .state('app.categoria', {
                url: "/categoria/:clase/:catid",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/categoria.html",
                        controller: 'CategoriaController'
                    }
                }
            })

            .state('app.municipios', {
                url: "/municipios",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/municipios.html",
                        controller: 'MunicipiosController'
                    }
                }
            })

            .state('app.municipio', {
                url: "/municipio/:clase/:munid",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/municipio.html",
                        controller: 'MunicipioController'
                    }
                }
            })

            .state('app.single', {
                url: "/playlists/:playlistId",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/playlist.html"
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/categorias');

    });

angular.module('agendatolima.controllers', []);

angular.module('agendatolima.services', []);

angular.module('agendatolima.providers', []);

angular.module('agendatolima.resources', []);
