angular.module('agendatolima.controllers').controller('AppController', function($window, $scope, Robot, ContextualService) {

    if (angular.isObject(JSON.parse(sessionStorage.getItem('agendatolima_usuario')))) {
        $scope.usuario = JSON.parse(sessionStorage.getItem('agendatolima_usuario'));
    } else {
        $scope.usuario = null;
    }

    $scope.$on('logged', function() {
        $scope.usuario = JSON.parse(sessionStorage.getItem('agendatolima_usuario'));
        Robot.alert("Bienvenido " + $scope.usuario.nombre);
    });

    $scope.$on('loggout', function() {

    });

    $scope.onCerrarSesionButtonClick = function() {
        sessionStorage.removeItem('agendatolima_usuario');
        $scope.usuario = null;
        Robot.alert("Se ha cerrado sesión satisfactoriamente");
    };

    var _initialize = function() {
        ContextualService.sincronizar(function () {
            $scope.cantidadCategorias = ContextualService.cantidades.categorias;
            $scope.cantidadMunicipios = ContextualService.cantidades.municipios;
            $scope.cantidadEventos = ContextualService.cantidades.eventos;
            $scope.cantidadLugares = ContextualService.cantidades.lugares;
        }, function () {
            Robot.alert("Se presentó un error intentando cargar las cantidades");
        });
    };

    _initialize();

});