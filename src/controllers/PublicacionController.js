angular.module('agendatolima.controllers').controller('PublicacionController', function($scope, $stateParams, $state, $window, $ionicGesture, $ionicLoading, $ionicModal, Robot, PublicacionesService, ComentariosService, SocialService, MapsService, remotehost) {

    var map,
        initialized = false;

    $scope.publicacion = {};

    $scope.comentario = {
        text: "",
        calificacion: 0
    };

    $scope.modalComentar = null;
    $scope.modalCalificar = null;

    var _initialize = function() {
        if (angular.isObject(JSON.parse(sessionStorage.getItem('agendatolima_usuario')))) {
            $scope.usuario = JSON.parse(sessionStorage.getItem('agendatolima_usuario'));
        } else {
            $scope.usuario = null;
        }

        $ionicModal.fromTemplateUrl('templates/modalComentar.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modalComentar = modal;
        });

        $ionicModal.fromTemplateUrl('templates/modalCalificar.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modalCalificar = modal;
        });

        _cargarPublicacion();
    };

    /**
     *
     * @private
     */
    var _initMap = function(lat, lng) {
        MapsService.cargarAPI(function() {
            var myLatlng = new google.maps.LatLng(lat, lng);

            var mapOptions = {
                zoom: 16,
                center: myLatlng
            };

            map = new google.maps.Map(document.getElementById('mapa-rutas'), mapOptions);

            map.panTo(myLatlng);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: $scope.publicacion.establecimiento
            });
        });

    };

    /**
     *
     * @private
     */
    var _cargarPublicacion = function() {
        $ionicLoading.show({template: "Cargando..."});

        PublicacionesService.retrieve($stateParams.id, function(pub) {
            pub.imagen = remotehost + "/files/publicaciones/" + pub.imagen;
            if (!pub.hasOwnProperty('fecha') || pub.fecha === "0000-00-00 00:00:00" || pub.fecha === "" || pub.fecha === null) {
                pub.fecha = "";
            } else {
                pub.fecha = moment(pub.fecha, "YYYY-MM-DD HH:mm:ss").format("D [de] MMMM [del] YYYY [a las] h:mm a");
            }
            if (pub.hasOwnProperty('municipio')) {
                pub.municipio = JSON.parse(pub.municipio).nombre;
            }
            $scope.publicacion = pub;
            if ($scope.publicacion.latitud !== '' && $scope.publicacion.longitud !== '') {
                _initMap($scope.publicacion.latitud, $scope.publicacion.longitud);
            }
            $ionicLoading.hide();
        }, function(error) {
            console.error(error);
            $ionicLoading.hide();
            Robot.alert("Se presentó un error intentando cargar las publicaciones: " + error.message);
        });
    };

    /**
     * @public
     * @handler
     */
    $scope.onCalificarButtonClick = function() {
        if (angular.isObject(JSON.parse(sessionStorage.getItem('agendatolima_usuario')))) {
            $scope.modalCalificar.show();
        } else {
            Robot.alert("Para calificar, debe primero iniciar sesión");
            $state.go('app.login');
        }
    };

    /**
     * @public
     * @handler
     */
    $scope.onComentarButtonClick = function() {
        if (angular.isObject(JSON.parse(sessionStorage.getItem('agendatolima_usuario')))) {
            $scope.modalComentar.show();
        } else {
            Robot.alert("Para comentar, debe primero iniciar sesión");
            $state.go('app.login');
        }
    };

    /**
     * @public
     * @handler
     */
    $scope.onCompartirButtonClick = function() {
        $ionicLoading.show({template: "Cargando..."});
        SocialService.compartir($scope.publicacion.titulo, $scope.publicacion.contenido.substring(0, 143), $scope.publicacion.imagen, function() {
            Robot.alert("Servicio no disponible en el dispositivo");
        });
        setTimeout(function() {
            $ionicLoading.hide();
        }, 3000);
    };

    /**
     *
     */
    $scope.onConfirmarCalificacionButtonClick = function(comentario) {
        ComentariosService.calificar(comentario.calificacion, $scope.publicacion.id, $scope.usuario.id, function(comentario) {
            Robot.alert("¡Calificación enviada satisfactoriamente!");
            $scope.modalCalificar.hide();
        }, function(error) {
            Robot.alert(error.mensaje || error.message);
        });
    };

    /**
     *
     */
    $scope.onConfirmarComentarioButtonClick = function(comentario) {
        ComentariosService.comentar(comentario.texto, $scope.publicacion.id, $scope.usuario.id, function(comentario) {
            Robot.alert("¡Comentario publicado satisfactoriamente!");
            $scope.modalComentar.hide();
            $scope.publicacion.comentarios.unshift(comentario);
        }, function(error) {
            Robot.alert(error.mensaje || error.message);
        });
    };

    _initialize();

});