angular.module('agendatolima.controllers').controller('SignupController', function($window, $scope, $state, Robot, UsuariosService) {

    $scope.user = {
        username: "",
        password: "",
        rpassword: "",
        nombre: "",
        apellidos: "",
        email: ""
    };

    $scope.crearCuenta = function(user) {
        if (user.password !== "" && user.username !== "" && user.email !== "" && user.nombre !== "") {
            if (user.password === user.rpassword) {
                delete user.rpassword;
                UsuariosService.registrar(user, function() {
                    Robot.alert("Usuario registrado satisfactoriamente");
                    $state.go('app.login');
                }, function(error) {
                    Robot.alert("Error intentando crear usuario");
                });
            } else {
                Robot.alert("La contraseña no coincide. Por favor digitela de nuevo");
                $scope.user.password = "";
                $scope.user.rpassword = "";
            }
        } else {
            Robot.alert("Por favor ingrese los datos requeridos (*)");
        }
    };

});