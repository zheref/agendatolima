angular.module('agendatolima.controllers').controller('MunicipioController', function($scope, $stateParams, $window, $ionicGesture, $ionicLoading, Robot, EventosService, LugaresService, remotehost) {

    // -----------------------------------------------------------------------------------
    // -------------------------------------- FIELDS -------------------------------------
    // -----------------------------------------------------------------------------------

    /**
     * @backs up the last well shown 'Publicacion' id
     * @type {number}
     * @private
     */
    var _backup = 1;

    $scope.indice = 1;
    $scope.publicacion = {};

    // -----------------------------------------------------------------------------------
    // -------------------------------------- METHODS ------------------------------------
    // -----------------------------------------------------------------------------------

    /**
     *
     * @private
     */
    var _initialize = function() {
        $ionicGesture.on('swipeleft', function() {
            $scope.onAnteriorButtonTap();
        }, angular.element('#publicacionesContainer'));

        _cargarPublicacion();
    };

    /**
     *
     * @private
     */
    var _cargarPublicacion = function() {
        var service;
        if ($stateParams.clase === "e") {
            service = EventosService;
        } else if ($stateParams.clase === "l") {
            service = LugaresService;
        } else {
            throw Error("Parameter for 'clase' not identified");
        }

        $ionicLoading.show({template: "Cargando..."});
        service.ordenadoMunicipio($stateParams.munid, $scope.indice, function(pub) {
            pub.imagen = remotehost + "/files/publicaciones/" + pub.imagen;
            if (!pub.hasOwnProperty('fecha') || pub.fecha === "0000-00-00 00:00:00" || pub.fecha === "" || pub.fecha === null) {
                pub.fecha = "";
            } else {
                pub.fecha = moment(pub.fecha, "YYYY-MM-DD HH:mm:ss").format("D [de] MMMM [del] YYYY [a las] h:mm a");
            }
            $scope.publicacion = pub;
            $ionicLoading.hide();
        }, function(error) {
            console.error(error);
            Robot.alert("Se presentó un error intentando cargar las publicaciones: " + error.message);
            if ($scope.indice > 1) {
                $scope.indice = 1;
            }
            $ionicLoading.hide();
        }, function() {
            $scope.indice = _backup;
        });
    };

    // -----------------------------------------------------------------------------------
    // ------------------------------------- HANDLERS ------------------------------------
    // -----------------------------------------------------------------------------------

    $scope.onAnteriorButtonTap = function() {
        if ($scope.publicacion.next === 'true') {
            _backup = $scope.indice;
            ++($scope.indice);
            _cargarPublicacion();
        }
    };

    $scope.onPosteriorButtonTap = function() {
        _backup = $scope.indice;
        if ($scope.indice !== 1) {
            --($scope.indice);
            _cargarPublicacion();
        }
    };

    _initialize();

});