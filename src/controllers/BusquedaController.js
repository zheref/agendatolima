angular.module('agendatolima.controllers').controller('BusquedaController', function($window, $scope, $ionicLoading, Robot, PublicacionesService, ContextualService, EventosService, LugaresService) {

    /**
     *
     * @type {{keywords: string}}
     */
    $scope.busqueda = {
        keywords: "",
        clase: "eventos",
        categoria: "0",
        municipio: "0"
    };

    $scope.categorias = [];
    $scope.municipios = [];

    /**
     *
     * @private
     */
    var _initialize = function() {
        _cargarUltimasPublicaciones();
        _cargarTaxonomias();
    };

    var _cargarUltimasPublicaciones = function() {
        $ionicLoading.show({template: "Cargando..."});

        PublicacionesService.ultimas(function(pubs) {
            $scope.publicaciones = pubs;
            $ionicLoading.hide();
        }, function(error) {
            $ionicLoading.hide();
            Robot.alert(error.mensaje);
        });
    };

    var _cargarTaxonomias = function() {
        $ionicLoading.show({template: "Cargando..."});

        if ($scope.busqueda.clase === "eventos") {
            ContextualService.taxonomiaEventos(function(taxonomias) {
                $scope.categorias = taxonomias.categorias;
                $scope.municipios = taxonomias.municipios;
                $ionicLoading.hide();
            }, function(error) {
                $ionicLoading.hide();
                Robot.alert(error.mensaje);
            });
        } else {
            ContextualService.taxonomiaLugares(function(taxonomias) {
                $scope.categorias = taxonomias.categorias;
                $scope.municipios = taxonomias.municipios;
                $ionicLoading.hide();
            }, function(error) {
                $ionicLoading.hide();
                Robot.alert(error.mensaje);
            });
        }
    };

    /**
     *
     * @param busqueda
     */
    $scope.onBuscarButtonClick = function(busqueda) {
        if (busqueda.keywords === "" || busqueda.keywords === " ") {
            busqueda.keywords = "*";
        }
        $ionicLoading.show({template: "Cargando..."});

        if ($scope.busqueda.clase === "eventos") {
            EventosService.buscar(busqueda.municipio, busqueda.categoria, busqueda.keywords, function(pubs) {
                $scope.publicaciones = pubs;
                $ionicLoading.hide();
            }, function(error) {
                $ionicLoading.hide();
                Robot.alert(error.mensaje);
            });
        } else {
            LugaresService.buscar(busqueda.municipio, busqueda.categoria, busqueda.keywords, function(pubs) {
                $scope.publicaciones = pubs;
                $ionicLoading.hide();
            }, function(error) {
                $ionicLoading.hide();
                Robot.alert(error.mensaje);
            });
        }
        if (busqueda.keywords = "*") {
            busqueda.keywords = "";
        }
    };

    /**
     *
     */
    $scope.onClaseRadioButtonChange = function() {
        _cargarTaxonomias();
    };

    _initialize();

});