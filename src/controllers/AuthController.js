angular.module('agendatolima.controllers').controller('AuthController', function($window, $scope, $state, Robot, UsuariosService) {

    $scope.user = {
        username: "",
        password: ""
    };

    /**
     *
     * @param user
     */
    $scope.iniciarSesion = function(user) {
        UsuariosService.validate(user.username, user.password, function(usuario) {
            $window.sessionStorage.setItem('agendatolima_usuario', JSON.stringify(usuario));
            $state.go('app.categorias');
            $scope.$emit('logged');
        }, function() {
            Robot.alert("Credenciales inválidas");
        }, function() {
            Robot.alert("Se presentó un error intentando autenticar el usuario");
        });
    };

});