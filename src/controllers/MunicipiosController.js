angular.module('agendatolima.controllers').controller('MunicipiosController', function($window, $scope, $ionicLoading, Robot, MunicipiosService) {

    $scope.municipiosEventos = [];
    $scope.municipiosLugares = [];

    var _initialize = function() {
        jQuery('.m-bellows').bellows();
        $ionicLoading.show({template: "Cargando..."});

        MunicipiosService.all(function(data) {
            $scope.municipiosEventos = data.municipiosEventos;
            $scope.municipiosLugares = data.municipiosLugares;
            $ionicLoading.hide();
        }, function(error) {
            $ionicLoading.hide();
            Robot.alert(error.message);
        });
    };

    _initialize();

});