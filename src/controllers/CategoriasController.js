angular.module('agendatolima.controllers').controller('CategoriasController', function($window, $scope, $ionicLoading, Robot, CategoriasService) {

    $scope.categoriasEventos = [];
    $scope.categoriasLugares = [];

    var _initialize = function() {
        jQuery('.m-bellows').bellows();
        $ionicLoading.show({template: "Cargando..."});

        CategoriasService.all(function(data) {
            $scope.categoriasEventos = data.categoriasEventos;
            $scope.categoriasLugares = data.categoriasLugares;
            $ionicLoading.hide();
        }, function(error) {
            $ionicLoading.hide();
            Robot.alert(error.message);
        });
    };

    _initialize();

});