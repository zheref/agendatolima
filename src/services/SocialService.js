angular.module('agendatolima.services').service('SocialService', [function() {

    /**
     * @public
     * @method
     * @param titulo
     * @param texto
     * @param imagen
     */
    this.compartir = function(titulo, texto, imagen, atNoDisponible) {
        if (window.hasOwnProperty('plugins')) {
            if (window.plugins.hasOwnProperty('socialsharing')) {
                window.plugins.socialsharing.share(texto, titulo, imagen, null);
            } else {
                atNoDisponible();
            }
        } else {
            atNoDisponible();
        }
    };

    return this;

}]);