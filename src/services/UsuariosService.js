angular.module('agendatolima.services').service('UsuariosService', ['$http', 'remotehost', function($http, remotehost) {

    this.validate = function(username, password, returner, atNotValid, atError) {
        jQuery.post(remotehost + "/ws/login", {'username': username, 'password': password})
            .done(function(data) {
                data = JSON.parse(data);
                if (data.hasOwnProperty('estado')) {
                    returner(data);
                } else if (data.hasOwnProperty('code')) {
                    if (data.code == 0) {
                        atNotValid();
                    }
                }
            })
            .fail(function(error) {
                console.error(error);
                atError(error);
            });
    };

    this.registrar = function(datos, atFinalizar, atError) {
        jQuery.post(remotehost + "/ws/registrar", datos)
            .done(function(data) {
                data = JSON.parse(data);
                if (data.hasOwnProperty('codigo')) {
                    if (data.codigo = "1") {
                        atFinalizar();
                    } else {
                        atError();
                    }
                } else {
                    atFinalizar();
                }
            })
            .fail(function(error) {
                console.error(error);
                atError(error);
            });
    };

    return this;

}]);