angular.module('agendatolima.services').service('ContextualService', ['$http', 'remotehost', function($http, remotehost) {
    var self = this;

    this.cantidades = {
        categorias: 0,
        municipios: 0,
        publicaciones: 0
    };

    this.taxonomiaEventos = function(atFinalizar, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/taxonomia-eventos"
        }).success(function(data) {
            if (!data.hasOwnProperty('codigo')) {
                atFinalizar(data);
            } else {
                atError(data);
            }
        }).error(function(data) {
            atError(data);
        });
    };

    this.taxonomiaLugares = function(atFinalizar, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/taxonomia-lugares"
        }).success(function(data) {
            if (!data.hasOwnProperty('codigo')) {
                atFinalizar(data);
            } else {
                atError(data);
            }
        }).error(function(data) {
            atError(data);
        });
    };

    this.sincronizar = function(atFinalizar, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/cantidades"
        }).success(function(data, status, headers, config) {
            self.cantidades = data;
            atFinalizar();
        }).error(function(data, status, headers, config) {
            atError(data);
        });
    };

    return this;

}]);