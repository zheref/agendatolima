angular.module('agendatolima.services').service('ComentariosService', ['$http', 'remotehost', function($http, remotehost) {

    /**
     *
     * @param comentario
     * @param publicacionId
     * @param usuarioId
     * @param returner
     * @param atError
     */
    this.comentar = function(comentario, publicacionId, usuarioId, returner, atError) {
        jQuery.post(remotehost + "/ws/comentario", {'comentario': comentario, 'publicacion_id': publicacionId, 'usuario_id': usuarioId})
            .done(function(data) {
                data = JSON.parse(data);
                if (data.hasOwnProperty('comentario')) {
                    returner(data);
                } else if (data.hasOwnProperty('code')) {
                    if (data.code == 0) {
                        atError(data);
                    }
                }
            })
            .fail(function(error) {
                console.error(error);
                atError(error);
            });
    };

    /**
     *
     * @param calificacion
     * @param publicacionId
     * @param usuarioId
     * @param returner
     * @param atError
     */
    this.calificar = function(calificacion, publicacionId, usuarioId, returner, atError) {
        jQuery.post(remotehost + "/ws/calificacion", {'comentario': "", 'calificacion': calificacion, 'publicacion_id': publicacionId, 'usuario_id': usuarioId})
            .done(function(data) {
                data = JSON.parse(data);
                if (data.hasOwnProperty('estado')) {
                    console.log(data);
                    returner(data);
                } else if (data.hasOwnProperty('code')) {
                    if (data.code == 0) {
                        atError(data);
                    }
                }
            })
            .fail(function(error) {
                console.error(error);
                atError(error);
            });
    };

    return this;

}]);