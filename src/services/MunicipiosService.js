angular.module('agendatolima.services').service('MunicipiosService', ['$http', 'remotehost', function($http, remotehost) {

    this.all = function(returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/taxonomia-municipios"
        }).success(function(data) {
            if (!data.hasOwnProperty('codigo')) {
                returner(data);
            } else {
                atError(data);
            }
        }).error(function(data) {
            atError(data);
        });
    };

    return this;

}]);