angular.module('agendatolima.services').service('MapsService', [function() {

    var API_KEY = "AIzaSyC05xJG8sH0C3_7zcPkW3PAv7l05M51jaw";

    /**
     * @public
     * @method
     * @param atFinish
     */
    this.cargarAPI = function(atFinish) {
        if (!window.GMAPS_READY) {
            window.callbackmap = function() {
                atFinish();
                delete window.callbackmap;
            };
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
                'key=' + API_KEY + '&callback=callbackmap';
            document.body.appendChild(script);
            window.GMAPS_READY = true;
        } else {
            atFinish();
        }
    };

    return this;

}]);