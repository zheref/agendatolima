angular.module('agendatolima.services').service('PublicacionesService', ['$window', '$http', 'remotehost', function($window, $http, remotehost) {

    this.ultimas = function(returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/ultimas-publicaciones"
        }).success(function(data) {
            returner(data);
        }).error(function(data) {
            console.error(data);
            atError(data);
        });
    };

    // ----------------------------------------------------------------------------------
    // BEFORE ---------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    this.all = function(returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/publicaciones"
        }).success(function(data, status, headers, config) {
            returner(data);
        }).error(function(data, status, headers, config) {
            alert("Se presentó un error intentando cargar las publicaciones");
            atError(data);
        });
    };

    this.ordenado = function(numero, returner, atError, atNoMore) {
        $http({
            method: "GET",
            url: remotehost + "/ws/publicacionchrono/" + numero
        }).success(function(data, status, headers, config) {
            if (data === "0") {
                atNoMore();
            } else {
                returner(data);
            }
        }).error(function(data, status, headers, config) {
            alert("Se presentó un error intentando cargar la publicacion");
            atError(data);
        });
    };

    this.ordenadoCategoria = function(categoriaId, numero, returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/publicacionchronocategoria/" + categoriaId + "/" + numero
        }).success(function(data, status, headers, config) {
            returner(data);
        }).error(function(data, status, headers, config) {
            alert("Se presentó un error intentando cargar la publicacion");
            atError(data);
        });
    };

    this.ordenadoMunicipio = function(municipioId, numero, returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/publicacionchronomunicipio/" + municipioId + "/" + numero
        }).success(function(data, status, headers, config) {
            returner(data);
        }).error(function(data, status, headers, config) {
            alert("Se presentó un error intentando cargar la publicacion");
            atError(data);
        });
    };

    this.retrieve = function(id, returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/publicacion/" + id
        }).success(function(data, status, headers, config) {
            returner(data);
        }).error(function(data, status, headers, config) {
            alert("Se presentó un error intentando cargar la publicacion");
            atError(data);
        });
    };

    this.buscar = function(keywords, returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/publicacionescadena/" + keywords
        }).success(function(data, status, headers, config) {
            returner(data);
        }).error(function(data, status, headers, config) {
            alert("Se presentó un error intentando cargar las publicaciones");
            atError(data);
        });
    };

    return this;

}]);