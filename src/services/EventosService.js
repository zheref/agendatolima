angular.module('agendatolima.services').service('EventosService', ['$http', 'remotehost', function($http, remotehost) {

    /**
     *
     * @param municipio_id
     * @param categoria_id
     * @param keywords
     * @param returner
     * @param atError
     */
    this.buscar = function(municipio_id, categoria_id, keywords, returner, atError) {
        $http({
            method: 'GET',
            url: remotehost + "/ws/eventos-municipio-categoria-keywords/" + municipio_id + "/" + categoria_id + "/" + keywords
        }).success(function(data) {
            if (!data.hasOwnProperty('codigo')) {
                returner(data);
            } else {
                atError(data);
            }
        }).error(function(data) {
            atError(data);
        });
    };

    /**
     *
     * @param orden
     * @param returner
     * @param atError
     */
    this.ordenado = function(orden, returner, atError) {
        $http({
            method: 'GET',
            url: remotehost + "/ws/evento-orden/" + orden
        }).success(function(data) {
            if (!data.hasOwnProperty('codigo')) {
                returner(data);
            } else {
                atError(data);
            }
        }).error(function(data) {
            atError(data);
        });
    };

    /**
     *
     * @param categoriaId
     * @param orden
     * @param returner
     * @param atError
     */
    this.ordenadoCategoria = function(categoriaId, orden, returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/evento-categoria-orden/" + categoriaId + "/" + orden
        }).success(function(data) {
            if (!data.hasOwnProperty('codigo')) {
                returner(data);
            } else {
                atError(data);
            }
        }).error(function(data) {
            alert("Se presentó un error intentando cargar la publicacion");
            atError(data);
        });
    };

    /**
     *
     * @param municipioId
     * @param orden
     * @param returner
     * @param atError
     */
    this.ordenadoMunicipio = function(municipioId, orden, returner, atError) {
        $http({
            method: 'GET',
            url: remotehost + "/ws/evento-municipio-orden/" + municipioId + "/" + orden
        }).success(function(data) {
            if (!data.hasOwnProperty('codigo')) {
                returner(data);
            } else {
                atError(data);
            }
        }).error(function(data) {
            atError(data);
        });
    };

    // ----------------------------------------------------------------------------------
    // BEFORE ---------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------

    this.all = function(returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/eventos"
        }).success(function(data) {
            returner(data);
        }).error(function(data) {
            alert("Se presentó un error intentando cargar las publicaciones");
            atError(data);
        });
    };

    this.ultimas = function(returner, atError) {
        $http({
            method: "GET",
            url: remotehost + "/ws/publicacionesultimas"
        }).success(function(data, status, headers, config) {
            returner(data);
        }).error(function(data, status, headers, config) {
            alert("Se presentó un error intentando cargar las publicaciones");
            atError(data);
        });
    };

    this.retrieve = function(id, returner, atError) {
        $http({
            method: 'GET',
            url: remotehost + "/ws/evento/" + id
        }).success(function(data) {
            returner(data);
        }).error(function(data) {
            atError(data);
        });
    };

    return this;

}]);