angular.module('agendatolima.providers').provider('Robot', function() {

    /**
     *
     * @returns {boolean}
     */
    this.isOnline = function() {
        if (navigator.hasOwnProperty('connection')) {
            var connectionType = navigator.connection.type;

            return connectionType === Connection.CELL ||
                connectionType === Connection.CELL_2G ||
                connectionType === Connection.CELL_3G ||
                connectionType === Connection.CELL_4G ||
                connectionType === Connection.WIFI ||
                connectionType === Connection.ETHERNET
        } else if (navigator.hasOwnProperty('onLine')) {
            return navigator.onLine;
        } else {
            return true;
        }
    };

    /**
     * @public
     * @async
     * @method
     * @param {String} title
     * @param {String} text
     * @param {String} confirmButtonText
     * @param {String} abortButtonText
     * @param {function()} atConfirmed
     * @param {function()=} atAborted
     */
    this.binaryConfirm = function(title, text, confirmButtonText, abortButtonText, atConfirmed, atAborted) {

        if (typeof (navigator.notification) !== 'undefined') {
            //noinspection JSUnresolvedVariable, JSUnresolvedFunction
            navigator.notification.confirm(text, function(buttonIndex) {
                if (buttonIndex === 1) {
                    atConfirmed();
                } else if (buttonIndex === 2) {
                    atAborted();
                } else {
                    var error = new Error("An unexpected and behavior-undefined index was detected");
                    console.error(error);
                    throw error;
                }
            }, title, [confirmButtonText, abortButtonText]);
        } else if (typeof (window.confirm) === 'function') {
            var buttonIndex = confirm(text);
            if (buttonIndex) {
                atConfirmed();
            } else {
                atAborted();
            }
        } else {
            throw new Error("Currently the binary confirm is just supported on top of Cordova, and Cordova couldn't be found");
        }
    };

    /**
     * @public
     * @async
     * @method
     * @param {string} title
     * @param {string} texto
     * @param {string} confirmButtonText
     * @param {string} cancelButtonText
     * @param {function(String)} resultCallback
     * @param {Function} atAborted
     */
    this.prompt = function (title, texto, confirmButtonText, cancelButtonText, resultCallback, atAborted) {

        if (typeof (navigator.notification) !== 'undefined') {
            //noinspection JSUnresolvedVariable
            navigator.notification.prompt(texto, function (response) {
                //noinspection JSUnresolvedVariable
                var buttonIndex = response.buttonIndex;
                if (buttonIndex === 1) {
                    //noinspection JSUnresolvedVariable
                    resultCallback(response.input1);
                } else if (buttonIndex === 2) {
                    atAborted();
                } else {
                    console.log("A non specified code was resolved: " + buttonIndex);
                }
            }, title, [confirmButtonText, cancelButtonText], String());
        } else {
            //noinspection JSUnresolvedFunction
            var input1 = window.prompt(texto, "NULL");
            if (input1 === "NULL") {
                atAborted();
            } else {
                resultCallback(input1);
            }
        }
    };

    this.alert = function (text, title, atOk) {
        atOk = atOk || function() {};
        title = title || "Alerta";

        if (typeof (navigator.notification) !== 'undefined') {
            //noinspection JSUnresolvedVariable
            navigator.notification.confirm(text, function(buttonIndex) {
                if (buttonIndex === 1) {
                    atOk();
                } else {
                    var error = new Error("An unexpected and behavior-undefined index was detected");
                    console.error(error);
                    throw error;
                }
            }, title, ["Ok"]);
        } else {
            window.alert(text, function () {
            }, "Alerta");
            atOk();
        }

    };

    this.$get =  function() {
        var self = this;

        return {
            isOnline: self.isOnline,
            binaryConfirm: self.binaryConfirm,
            prompt: self.prompt,
            alert: self.alert
        };
    };

});