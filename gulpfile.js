var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');

var paths = {
  sass: ['./scss/**/*.scss'],
    js: ['./src/**/*.js']
};

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('bb-concat-app', function() {
    gulp.src('./src/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./www/js/'));
});

gulp.task('bb-concat-controllers', function() {
    gulp.src('./src/controllers/*.js')
        .pipe(concat('controllers.js'))
        .pipe(gulp.dest('./www/js/'));
});

gulp.task('bb-concat-models', function() {
    gulp.src('./src/models/*.js')
        .pipe(concat('models.js'))
        .pipe(gulp.dest('./www/js/'));
});

gulp.task('bb-concat-services', function() {
    gulp.src('./src/services/*.js')
        .pipe(concat('services.js'))
        .pipe(gulp.dest('./www/js/'));
});

gulp.task('bb-concat-providers', function() {
    gulp.src('./src/providers/*.js')
        .pipe(concat('providers.js'))
        .pipe(gulp.dest('./www/js/'));
});

gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.js, ['bb-concat']);
});

gulp.task('bb-concat', ['bb-concat-app', 'bb-concat-controllers', 'bb-concat-models', 'bb-concat-services', 'bb-concat-providers']);

gulp.task('default', ['sass', 'bb-concat']);